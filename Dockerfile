FROM node:12-stretch AS builder

# Create app directory
WORKDIR /opt/turnip-stonks
RUN mkdir ./tmp

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./
RUN npm ci --only=production


FROM node:12-slim
WORKDIR /opt/turnip-stonks
RUN mkdir ./tmp
ENV TYPEORM_ENTITIES="./build/api/models/**/*.js"
ENV TYPEORM_CACHE="true"
ENV TYPEORM_MIGRATIONS="./build/migration/*.js"
#RUN npm install
# If you are building your code for production
COPY --from=builder /opt/turnip-stonks/node_modules ./node_modules
COPY ./build ./build
COPY package*.json ./

EXPOSE 3000
CMD ["npm", "start"]
