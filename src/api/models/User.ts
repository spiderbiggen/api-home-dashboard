import { compare, hash } from 'bcrypt';
import { Column, Entity, getRepository, OneToMany } from 'typeorm';
import md5 from 'md5';
import { DatedEntity } from './DatedEntity';
import { RefreshToken } from './RefreshToken';
import { OAuth } from './OAuth';

/**
 * Entity that holds information about a user.
 *
 * @author Stefan Breetveld
 */
@Entity('users')
export class User extends DatedEntity {

  @Column({ nullable: false, unique: true })
  username: string;
  @Column({ unique: true })
  email: string;
  @Column({ nullable: false, select: false })
  password: string; // technically a hash of the password.
  @Column('simple-array')
  roles?: string[];
  @OneToMany(() => RefreshToken, token => token.user)
  tokens: RefreshToken[];
  @OneToMany(() => OAuth, oauth => oauth.user)
  services: OAuth[];

  static get repository() {
    return getRepository(User);
  }

  static async isAdmin(id?: string): Promise<boolean> {
    const user = await User.repository.findOne(id);
    return user && user.roles ? user.roles.includes('admin') : false;
  }

  static async authorize(email: string, password: string): Promise<User | null> {
    const user = await User.repository.findOne({email: email.toLowerCase()}, {select: ['password']});
    if (user && await compare(password, user.password)) {
      return await User.repository.findOne({email: email.toLowerCase()}) ?? null;
    }
    return null;
  }

  static async createUser(username: string, email: string, password: string, roles ?: string[]): Promise<User> {
    const pass = await hash(password, 14);
    const repo = User.repository;
    const u = repo.create({username, password: pass, email: email.toLowerCase(), roles});
    return await repo.save(u);
  }

  toJSON(): any {
    return {
      id: this.id,
      username: this.username,
      email: this.email,
      roles: this.roles,
      avatar: `//www.gravatar.com/avatar/${md5(this.email.toLowerCase())}?d=identicon`,
    };
  }

}
