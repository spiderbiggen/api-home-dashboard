import { Column, Entity, getRepository, ManyToOne, PrimaryColumn } from 'typeorm';
import { User } from './User';
import { Services } from './Services';
import moment from 'moment';
import { ToonController } from '../../modules/toon/ToonController';
import { ForbiddenError } from '../../util';

/**
 * Entity that holds information about a user.
 *
 * @author Stefan Breetveld
 */
@Entity('oauth')
export class OAuth {
  private static _services: { [key: string]: RefreshFunction } = {};

  static registerService(key: string, fn: RefreshFunction) {
    OAuth._services[key] = fn;
  }

  @PrimaryColumn({ type: 'varchar', length: 32 })
  service: string;
  @ManyToOne<User>(() => User, user => user.services, { primary: true })
  user: User;
  @Column()
  access_token: string;
  @Column({ nullable: true })
  refresh_token?: string;
  @Column('timestamp', { nullable: true })
  expires?: Date;
  @Column('timestamp', { nullable: true })
  refresh_expires?: Date;

  static get repository() {
    return getRepository(OAuth);
  }

  static async getToken(userOrId: User | string, service: string): Promise<string | null> {
    const user: User = userOrId instanceof User ? userOrId : await User.repository.findOneOrFail(userOrId);
    let oAuth = await this.repository.findOne({ user: user, service: service });
    if (!oAuth) {
      throw new ForbiddenError();
    }
    if (moment().add(30, 'seconds').isSameOrAfter(moment(oAuth.expires))) {
      if (oAuth.service === Services.TOON) {
        oAuth = await ToonController.refresh(user, oAuth);
      }
    }
    if (!oAuth) {
      throw new ForbiddenError();
    }
    return oAuth.access_token;
  }

  toJSON(): any {
    return {
      user: this.user,
    };
  }
}

export type RefreshFunction = (user: User, oAuth: OAuth) => Promise<OAuth>;
