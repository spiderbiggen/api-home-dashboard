import UserRoutes from './UserRoutes';
import ToonRoutes from '../../modules/toon/ToonRoutes';

/**
 * Export a list of all routes
 */
export default [UserRoutes, ToonRoutes];
