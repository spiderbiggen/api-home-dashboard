import {MigrationInterface, QueryRunner} from "typeorm";

export class RefreshToken1578244663757 implements MigrationInterface {
  name = 'RefreshToken1578244663757';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`CREATE TABLE "public"."refresh_tokens" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "token" character varying(512) NOT NULL, "expires" TIMESTAMP NOT NULL, "disabled" boolean NOT NULL DEFAULT false, "user_id" uuid REFERENCES "users"("id") ON DELETE CASCADE , CONSTRAINT "UQ_4856449925e1c78455f9dc2e25a" UNIQUE ("token"), CONSTRAINT "PK_e6e34a3c663c3efcdfdaa3aeeb6" PRIMARY KEY ("id"))`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP TABLE "public"."refresh_tokens"`, undefined);
  }

}
