import { MigrationInterface, QueryRunner } from 'typeorm';
import { User } from '../api/models';

export class DefaultUser1578244663758 implements MigrationInterface {
  name = 'DefaultUser1578244663758';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`INSERT INTO public.users("id", "created_at", "updated_at", "username", "email", "password", "roles") VALUES (DEFAULT, DEFAULT, DEFAULT, $1, $2, $3, $4) RETURNING "id", "created_at", "updated_at"`, ['admin', 'webmaster@spiderbiggen.com', '$2b$14$ioY0b6FgpILncmwJMGE1V.SMsBz/W7uerqnHRbLpOFxhgrbvRluES', [ 'admin' ]]);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DELETE FROM public.users where username=$1`, ['admin']);
  }

}
