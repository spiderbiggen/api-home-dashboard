import {MigrationInterface, QueryRunner} from "typeorm";
import { User } from '../api/models';

export class initial1583595208844 implements MigrationInterface {
    name = 'initial1583595208844'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "public"."oauth" ("service" character varying(32) NOT NULL, "access_token" character varying NOT NULL, "refresh_token" character varying, "expires" TIMESTAMP, "refresh_expires" TIMESTAMP, "userId" uuid NOT NULL, CONSTRAINT "PK_e540393dcecbe734983e228cdef" PRIMARY KEY ("service", "userId"))`, undefined);
        await queryRunner.query(`ALTER TABLE "oauth" ADD CONSTRAINT "FK_639b5775145fb76279ffa6f0ee5" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."oauth" DROP CONSTRAINT "FK_639b5775145fb76279ffa6f0ee5"`, undefined);
        await queryRunner.query(`DROP TABLE "public"."oauth"`, undefined);
    }

}
