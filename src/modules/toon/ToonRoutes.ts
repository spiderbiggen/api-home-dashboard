import { RouteWrapper } from '../../routing';
import { Authentication } from '../../util';
import { ToonController } from './ToonController';

const router = new RouteWrapper('/toon');
router.post('/callback', ToonController.webhookCallBack)
const authenticatedRouter = new RouteWrapper('/');
authenticatedRouter.registerMiddleware(Authentication.verifyAuthentication);
authenticatedRouter.post('/token', ToonController.getToken);
authenticatedRouter.get('/agreements', ToonController.getAgreements);

authenticatedRouter.get('/:agreement_id/status', ToonController.getStatus);
authenticatedRouter.get('/:agreement_id/thermostat', ToonController.getTemperature);
authenticatedRouter.put('/:agreement_id/thermostat', ToonController.setTemperature);
authenticatedRouter.post('/:agreement_id/subscribe', ToonController.subscribe);
const consumption = new RouteWrapper('/consumption');
consumption.get('/:agreement_id/:energy_type/:data_type/', ToonController.getConsumption);

authenticatedRouter.subRoutes(consumption);
router.subRoutes(authenticatedRouter);
export default router;
