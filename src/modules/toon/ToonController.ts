import { Request, Response } from 'express';
import { BadRequestError, ForbiddenError, LOGGER } from '../../util';
import { OAuth, Services, User } from '../../api/models';
import moment from 'moment';
import { HttpService } from '../../network';
import { URLSearchParams } from 'url';
import { Agreement, FlowResponse, StatusResponse, TokenResponse, WebhookResponse } from './ToonResponseModels';
import { AxiosRequestConfig } from 'axios';
import ToonSocket from './ToonSocket';

const TOON_KEY = process.env.TOON_KEY;
const TOON_SECRET = process.env.TOON_SECRET;


/**
 * Module that has all resources for interacting with the toon api.
 *
 * @author Stefan Breetveld
 */
export module ToonController {
  const FRONTEND_BASE_URL = process.env.FRONTEND_BASE_URL || 'https://localhost:8080';
  const AuthService = new HttpService('https://api.toon.eu/', 'application/x-www-form-urlencoded');
  const RequestService = new HttpService('https://api.toon.eu/toon/v3/');


  export async function getToken(req: Request, res: Response): Promise<Agreement[]> {
    const { code }: { code: string } = req.body;
    if (!code) {
      throw new BadRequestError('code must be provided');
    }

    const data = {
      client_id: TOON_KEY,
      client_secret: TOON_SECRET,
      grant_type: 'authorization_code',
      issuer: 'identity.toon.eu',
      redirect_uri: `${FRONTEND_BASE_URL}/toon/callback`,
      tenant_id: 'eneco',
      code: code,
    };
    const user = await User.repository.findOneOrFail(res.locals.user);
    await doTokenRequest(user, data);
    return getAgreements(req, res);
  }

  export async function refresh(user: User, oAuth: OAuth): Promise<OAuth> {
    const data = {
      client_id: TOON_KEY,
      client_secret: TOON_SECRET,
      grant_type: 'refresh_token',
      refresh_token: oAuth.refresh_token,
    };
    return doTokenRequest(user, data);
  }

  async function doTokenRequest(user: User, data: any): Promise<OAuth> {
    const serviceRepository = OAuth.repository;
    let response: TokenResponse;
    try {
      response = await AuthService.post<TokenResponse>('token', new URLSearchParams(data));
    } catch (e) {
      // FIXME use 401 with special error
      throw new ForbiddenError();
    }
    const entity = await serviceRepository.save({
      user: user,
      service: Services.TOON,
      access_token: response.access_token,
      expires: moment().add(Number(response.expires_in), 'seconds').toDate(),
      refresh_token: response.refresh_token,
      refresh_expires: response.refresh_token_expires_in && moment().add(Number(response.refresh_token_expires_in), 'second').toDate(),
    });
    if (!entity) {
      throw new Error();
    }
    return entity;
  }

  export async function getAgreements(req: Request, res: Response): Promise<Agreement[]> {
    const token = await OAuth.getToken(res.locals.user, Services.TOON);
    return RequestService.get<Agreement[]>('/agreements', { headers: { authorization: `Bearer ${ token }` } });
  }

  export async function getConsumption(req: Request, res: Response): Promise<FlowResponse> {
    const token = await OAuth.getToken(res.locals.user, Services.TOON);
    let { agreement_id, energy_type = 'electricity', data_type = 'flows' } = req.params;
    if (!agreement_id) {
      throw new BadRequestError();
    }
    const { from, to } = req.query;
    const config: AxiosRequestConfig = {
      params: {
        fromTime: from && (moment(from).unix() * 1000),
        toTime: to && (moment(to).unix() * 1000),
      },
      headers: { authorization: `Bearer ${ token }` },
    };
    return RequestService.get<FlowResponse>(`/${ agreement_id }/consumption/${ energy_type }/${ data_type }`, config);
  }

  export async function getStatus(req: Request, res: Response): Promise<StatusResponse> {
    const token = await OAuth.getToken(res.locals.user, Services.TOON);
    let { agreement_id } = req.params;
    if (!agreement_id) {
      throw new BadRequestError();
    }
    const config: AxiosRequestConfig = {
      headers: { authorization: `Bearer ${ token }` },
    };
    return RequestService.get<StatusResponse>(`/${ agreement_id }/status`, config);
  }

  export async function getTemperature(req: Request, res: Response): Promise<unknown> {
    const token = await OAuth.getToken(res.locals.user, Services.TOON);
    let { agreement_id } = req.params;
    if (!agreement_id || !req.body.target) {
      throw new BadRequestError();
    }
    const config: AxiosRequestConfig = {
      headers: { authorization: `Bearer ${ token }` },
    };
    return RequestService.get<StatusResponse>(`/${ agreement_id }/thermostat`, config);
  }

  export async function setTemperature(req: Request, res: Response): Promise<unknown> {
    const token = await OAuth.getToken(res.locals.user, Services.TOON);
    let { agreement_id } = req.params;
    if (!agreement_id) {
      throw new BadRequestError();
    }
    const config: AxiosRequestConfig = {
      headers: { authorization: `Bearer ${ token }` },
    };
    return RequestService.put<StatusResponse>(`/${ agreement_id }/thermostat`, req.body, config);
  }

  export async function webhookCallBack(req: Request, res: Response): Promise<unknown> {
    const body: WebhookResponse = req.body;
    LOGGER.debug(`Toon webhook received: [${ Object.keys(body.updateDataSet).join(', ') }]`);
    if (body.updateDataSet.gasUsage) {
      ToonSocket.publish('gas', body.commonName, body.updateDataSet.gasUsage);
    }
    if (body.updateDataSet.thermostatInfo) {
      ToonSocket.publish('thermostat', body.commonName, body.updateDataSet.thermostatInfo);
    }
    if (body.updateDataSet.powerUsage) {
      ToonSocket.publish('power', body.commonName, body.updateDataSet.powerUsage);
    }
    return;
  }

  export async function subscribe(req: Request, res: Response) {
    const token = await OAuth.getToken(res.locals.user, Services.TOON);
    let { agreement_id } = req.params;
    if (!agreement_id || !token) {
      throw new BadRequestError();
    }
    const config: AxiosRequestConfig = {
      headers: {
        authorization: `Bearer ${ token }`,
        'cache-control': 'no-cache',
        'X-Agreement-ID': agreement_id,
      },
    };
    const data = {
      applicationId: TOON_KEY,
      callbackUrl: 'https://api.spiderbiggen.com/api-home/toon/callback',
      subscribedActions: [
        'PowerUsage',
        'Thermostat',
        'BoilerErrorInfo',
      ],
    };
    await unsubscribeAll(token, agreement_id);
    await RequestService.post<unknown>(`/${ agreement_id }/webhooks`, data, config);
  }

  async function unsubscribeAll(token: string, agreement_id: string) {
    const config: AxiosRequestConfig = {
      headers: {
        authorization: `Bearer ${ token }`,
        'cache-control': 'no-cache',
        'X-Agreement-ID': agreement_id,
      },
    };
    await RequestService.delete<unknown>(`/${ agreement_id }/webhooks/${ TOON_KEY }`, config);
  }

}
