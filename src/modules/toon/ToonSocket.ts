import { Namespace, Server } from 'socket.io';
import { LOGGER } from '../../util';

class ToonSocket /* extends socketmodule */ {
  private socket: Server;
  private namespace: Namespace;

  init(socket: Server) {
    this.socket = socket;
    this.namespace = socket.of('/toon');
    this.namespace.on('connection', socket1 => {
      LOGGER.verbose(`New Socket connection to namespace /toon with id ${socket1.id}`)
      socket1.on('join', (a: any) => socket1.join(a))
    })
    LOGGER.verbose('Initialised /toon namespace')
  }

  publish(event: string, room?: string, ...data: any[]) {
    if (!this.namespace) {
      LOGGER.warn('Toon Websocket namespace was not initialized');
      return;
    }

    const socket = !!room ? this.namespace.in(room) : this.namespace;
    socket.emit(event, data);
  }

}

export default new ToonSocket();
