import Axios, { AxiosRequestConfig, AxiosInstance } from 'axios';
import { LOGGER, UnauthorizedError } from '../util';

export class HttpService {
  private readonly instance: AxiosInstance;

  constructor(baseUrl: string, contentType: string = 'application/json') {
    if (baseUrl == null) {
      throw new Error('baseUrl cannot be null');
    }
    this.instance = Axios.create({
      baseURL: baseUrl,
      headers: {
        'Content-Type': contentType,
      },
    });
    this.instance.interceptors.response.use(value => value, error => {
      LOGGER.error(JSON.stringify(error, null, 2));
      LOGGER.error(JSON.stringify(error.response.headers, null, 2));
      LOGGER.error(JSON.stringify(error.response.data, null, 2));
      if (error.response?.status === 401) {
        throw new UnauthorizedError();
      }
      throw error;
    });
  }

  get interceptors() {
    return this.instance.interceptors;
  }

  get headers() {
    return this.instance.defaults.headers;
  }

  async head<T = any>(url: string, config?: AxiosRequestConfig): Promise<T> {
    return (await this.instance.head<T>(url, config)).data;
  }

  async get<T = any>(url: string, config?: AxiosRequestConfig): Promise<T> {
    return (await this.instance.get<T>(url, config)).data;
  }

  async post<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
    return (await this.instance.post<T>(url, data, config)).data;
  }

  async patch<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
    return (await this.instance.patch<T>(url, data, config)).data;
  }

  async put<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
    return (await this.instance.put<T>(url, data, config)).data;
  }

  async delete<T = any>(url: string, config?: AxiosRequestConfig): Promise<T> {
    return (await this.instance.delete<T>(url, config)).data;
  }
}
