import Socket from 'socket.io';
import { Server } from 'http';
import Sockets from '../api/websockets';

class WebSocket {
  private _socket?: Socket.Server;

  get socket() {
    return this._socket;
  }

  init(server: Server, basePath: string) {
    const socket = Socket(server, {path: `${basePath}/socket.io`});
    this._socket = socket;
    Sockets.forEach(s => s.init(socket));
  }
}

export default new WebSocket();
